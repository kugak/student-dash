﻿using System.Web.Configuration;
using Twilio;
using Twilio.Clients;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace StudentDash
{
    public class RestClient
    {
        private readonly ITwilioRestClient _client;
        private readonly string _accountSid = "AC774d9eb39a14330ab071dd9985ea159a";
        private readonly string _authToken = "db49bd5786482adaf2424141c6e1a946";
        private readonly string _twilioNumber = "6473608002";

        public RestClient()
        {
            _client = new TwilioRestClient(_accountSid, _authToken);
        }

        public RestClient(ITwilioRestClient client)
        {
            _client = client;
        }

        public void SendSmsMessage(string phoneNumber, string message)
        {
            TwilioClient.Init(_accountSid, _authToken);

            var to = new PhoneNumber(phoneNumber);
             MessageResource.Create(
                to,
                from: new PhoneNumber("+16473608002"),
                body: message);
        }
    }
}
