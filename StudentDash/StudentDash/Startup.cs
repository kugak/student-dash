﻿using Hangfire;
using IdentitySample.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using StudentDash.Controllers;
using System;

namespace IdentitySample
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            EventsController ev = new EventsController();
            GlobalConfiguration.Configuration.UseSqlServerStorage("data source=(localdb)\\MSSQLLocalDB;initial catalog=sd-db;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework");

            RecurringJob.AddOrUpdate(() => ev.Execute(), Cron.Minutely);

            app.UseHangfireDashboard();

            app.UseHangfireServer();
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));



            // In Startup iam creating first Admin Role and creating a default Admin User 
            if (!roleManager.RoleExists("Admin"))
            {

                // first we create Admin rool
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);


            }
            var user = new ApplicationUser { UserName = "admin@gmail.com", Email = "admin@gmail.com" };
            var result = UserManager.Create(user, "Admin_1");
            if (result.Succeeded)
            {
                var result1 = UserManager.AddToRole(user.Id, "Admin");

            }

        }
    }
}
