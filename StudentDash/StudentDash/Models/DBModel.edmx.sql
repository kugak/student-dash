
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/28/2017 16:02:42
-- Generated from EDMX file: \\vmware-host\Shared Folders\Desktop\studentdash\student-dash\StudentDash\StudentDash\Models\DBModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [sd-db];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_CourseGrade]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Grades] DROP CONSTRAINT [FK_CourseGrade];
GO
IF OBJECT_ID(N'[dbo].[FK_StudentToDoList]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ToDoLists] DROP CONSTRAINT [FK_StudentToDoList];
GO
IF OBJECT_ID(N'[dbo].[FK_ProgramCourse]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Courses] DROP CONSTRAINT [FK_ProgramCourse];
GO
IF OBJECT_ID(N'[dbo].[FK_StudentEvent]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Events] DROP CONSTRAINT [FK_StudentEvent];
GO
IF OBJECT_ID(N'[dbo].[FK_StudentProgram_Student]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StudentProgram] DROP CONSTRAINT [FK_StudentProgram_Student];
GO
IF OBJECT_ID(N'[dbo].[FK_StudentProgram_Program]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StudentProgram] DROP CONSTRAINT [FK_StudentProgram_Program];
GO
IF OBJECT_ID(N'[dbo].[FK_StudentGrade]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Grades] DROP CONSTRAINT [FK_StudentGrade];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Students]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Students];
GO
IF OBJECT_ID(N'[dbo].[Courses]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Courses];
GO
IF OBJECT_ID(N'[dbo].[Grades]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Grades];
GO
IF OBJECT_ID(N'[dbo].[Programs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Programs];
GO
IF OBJECT_ID(N'[dbo].[ToDoLists]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ToDoLists];
GO
IF OBJECT_ID(N'[dbo].[Events]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Events];
GO
IF OBJECT_ID(N'[dbo].[StudentProgram]', 'U') IS NOT NULL
    DROP TABLE [dbo].[StudentProgram];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Students'
CREATE TABLE [dbo].[Students] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(255)  NOT NULL,
    [LastName] nvarchar(255)  NOT NULL,
    [CurrentGPA] float  NULL
);
GO

-- Creating table 'Courses'
CREATE TABLE [dbo].[Courses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(255)  NOT NULL,
    [Code] nvarchar(25)  NOT NULL,
    [Credits] float  NOT NULL,
    [ProgramId] int  NULL,
    [CurrentGrade] float  NOT NULL
);
GO

-- Creating table 'Grades'
CREATE TABLE [dbo].[Grades] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [GradeName] nvarchar(255)  NOT NULL,
    [GradeType] nvarchar(255)  NOT NULL,
    [TotalMarks] float  NOT NULL,
    [AcheivedMarks] float  NULL,
    [Weight] float  NOT NULL,
    [CourseId] int  NOT NULL,
    [StudentId] int  NULL
);
GO

-- Creating table 'Programs'
CREATE TABLE [dbo].[Programs] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(255)  NOT NULL,
    [School] nvarchar(255)  NOT NULL,
    [Semester] int  NOT NULL,
    [SemStartDate] datetime  NOT NULL,
    [SemEndDate] datetime  NOT NULL,
    [StudentId] int  NULL
);
GO

-- Creating table 'ToDoLists'
CREATE TABLE [dbo].[ToDoLists] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ItemName] nvarchar(255)  NOT NULL,
    [Desc] nvarchar(255)  NULL,
    [DueDate] datetime  NOT NULL,
    [StartDate] datetime  NULL,
    [EndDate] datetime  NULL,
    [IsComplete] bit  NULL,
    [StudentId] int  NULL
);
GO

-- Creating table 'Events'
CREATE TABLE [dbo].[Events] (
    [EventID] int IDENTITY(1,1) NOT NULL,
    [Subject] nvarchar(100)  NOT NULL,
    [Description] nvarchar(300)  NULL,
    [Location] nvarchar(300)  NULL,
    [RoomNo] nvarchar(300)  NULL,
    [Start] datetime  NOT NULL,
    [End] datetime  NULL,
    [ThemeColor] nvarchar(10)  NULL,
    [IsFullDay] bit  NOT NULL,
    [StudentId] int  NOT NULL,
    [PhoneNumber] nvarchar(50)  NULL,
    [IsReminded] bit  NULL
);
GO

-- Creating table 'StudentProgram'
CREATE TABLE [dbo].[StudentProgram] (
    [Students_Id] int  NOT NULL,
    [Programs_Id] int  NOT NULL
);
GO

-- Creating table 'StudentGrade'
CREATE TABLE [dbo].[StudentGrade] (
    [Students_Id] int  NOT NULL,
    [Grades_Id] int  NOT NULL,
	[AcheivedMarks] int  NULL,
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Students'
ALTER TABLE [dbo].[Students]
ADD CONSTRAINT [PK_Students]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Courses'
ALTER TABLE [dbo].[Courses]
ADD CONSTRAINT [PK_Courses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Grades'
ALTER TABLE [dbo].[Grades]
ADD CONSTRAINT [PK_Grades]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Programs'
ALTER TABLE [dbo].[Programs]
ADD CONSTRAINT [PK_Programs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ToDoLists'
ALTER TABLE [dbo].[ToDoLists]
ADD CONSTRAINT [PK_ToDoLists]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [EventID] in table 'Events'
ALTER TABLE [dbo].[Events]
ADD CONSTRAINT [PK_Events]
    PRIMARY KEY CLUSTERED ([EventID] ASC);
GO

-- Creating primary key on [Students_Id], [Programs_Id] in table 'StudentProgram'
ALTER TABLE [dbo].[StudentProgram]
ADD CONSTRAINT [PK_StudentProgram]
    PRIMARY KEY CLUSTERED ([Students_Id], [Programs_Id] ASC);
GO

-- Creating primary key on [Students_Id], [Grades_Id] in table 'StudentGrade'
ALTER TABLE [dbo].[StudentGrade]
ADD CONSTRAINT [PK_StudentGrade]
    PRIMARY KEY CLUSTERED ([Students_Id], [Grades_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [CourseId] in table 'Grades'
ALTER TABLE [dbo].[Grades]
ADD CONSTRAINT [FK_CourseGrade]
    FOREIGN KEY ([CourseId])
    REFERENCES [dbo].[Courses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CourseGrade'
CREATE INDEX [IX_FK_CourseGrade]
ON [dbo].[Grades]
    ([CourseId]);
GO

-- Creating foreign key on [StudentId] in table 'ToDoLists'
ALTER TABLE [dbo].[ToDoLists]
ADD CONSTRAINT [FK_StudentToDoList]
    FOREIGN KEY ([StudentId])
    REFERENCES [dbo].[Students]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StudentToDoList'
CREATE INDEX [IX_FK_StudentToDoList]
ON [dbo].[ToDoLists]
    ([StudentId]);
GO

-- Creating foreign key on [ProgramId] in table 'Courses'
ALTER TABLE [dbo].[Courses]
ADD CONSTRAINT [FK_ProgramCourse]
    FOREIGN KEY ([ProgramId])
    REFERENCES [dbo].[Programs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProgramCourse'
CREATE INDEX [IX_FK_ProgramCourse]
ON [dbo].[Courses]
    ([ProgramId]);
GO

-- Creating foreign key on [StudentId] in table 'Events'
ALTER TABLE [dbo].[Events]
ADD CONSTRAINT [FK_StudentEvent]
    FOREIGN KEY ([StudentId])
    REFERENCES [dbo].[Students]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StudentEvent'
CREATE INDEX [IX_FK_StudentEvent]
ON [dbo].[Events]
    ([StudentId]);
GO

-- Creating foreign key on [Students_Id] in table 'StudentProgram'
ALTER TABLE [dbo].[StudentProgram]
ADD CONSTRAINT [FK_StudentProgram_Student]
    FOREIGN KEY ([Students_Id])
    REFERENCES [dbo].[Students]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Programs_Id] in table 'StudentProgram'
ALTER TABLE [dbo].[StudentProgram]
ADD CONSTRAINT [FK_StudentProgram_Program]
    FOREIGN KEY ([Programs_Id])
    REFERENCES [dbo].[Programs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StudentProgram_Program'
CREATE INDEX [IX_FK_StudentProgram_Program]
ON [dbo].[StudentProgram]
    ([Programs_Id]);
GO

-- Creating foreign key on [Students_Id] in table 'StudentGrade'
ALTER TABLE [dbo].[StudentGrade]
ADD CONSTRAINT [FK_StudentGrade_Student]
    FOREIGN KEY ([Students_Id])
    REFERENCES [dbo].[Students]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Grades_Id] in table 'StudentGrade'
ALTER TABLE [dbo].[StudentGrade]
ADD CONSTRAINT [FK_StudentGrade_Grade]
    FOREIGN KEY ([Grades_Id])
    REFERENCES [dbo].[Grades]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StudentGrade_Grade'
CREATE INDEX [IX_FK_StudentGrade_Grade]
ON [dbo].[StudentGrade]
    ([Grades_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------