﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentDash.Models
{
    public class GradeVM
    {
        public Grade grade { get; set; }

        public List<Student> StudentList { get; set; }

        public GradeVM()
        {
            StudentList = new List<Student>();
        }
    }
}