﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IdentitySample.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using StudentDash.Models;

namespace StudentDash.Controllers
{
    public class StudentsController : Controller
    {
        private DBModelContainer db = new DBModelContainer();
        public ActionResult getStudentGrades()
        {
            Student st = db.Students.Find(Session["StudentId"]);
            Program p= st.Programs.First();
            List<Course> courses = new List<Course>();
            courses = p.Courses.ToList();
            foreach(Course c in courses)
            {
                int weight = 0;
                int totalmarks = 0;
                int acheivedmarks = 0;
                List<Grade> grades = c.Grades.ToList();
                List<StudentGrade> studentgradeList = new List<StudentGrade>();
                studentgradeList = db.StudentGrades.ToList();
                foreach (Grade g in grades)
                {
                    foreach (StudentGrade sg in studentgradeList)
                    {

                        if (g.Id == sg.Grades_Id && st.Id == sg.Students_Id)
                        {
                            //gr.AcheivedMarks = sg.AcheivedMarks;
                            acheivedmarks = acheivedmarks + (int)sg.AcheivedMarks;
                        }
                        weight = weight + (int)g.Weight; // for example 20 out of 100
                        totalmarks = totalmarks + (int)g.TotalMarks;
                        // acheivedmarks = acheivedmarks + (int)g.AcheivedMarks;

                    }
                }
                if (totalmarks == 0) { totalmarks = 1; }
                decimal final = ((decimal)acheivedmarks/(decimal)totalmarks)*(decimal)weight;
                c.CurrentGrade = Math.Round((double)final,2);
                db.SaveChanges();
            }
            return PartialView(courses);
        }

        //This method calculate gpa using courses current grade
        public ActionResult getStudentGpa()
        {

            int StudentId = (int)Session["StudentId"];
            Student st = new Student();
            // find student in database
            st = db.Students.Find(StudentId);
            if (st != null)
            {
                //get student program 
                Program pr = new Program();
                pr = st.Programs.First();
                if (pr != null)
                {
                    List<Course> courses = new List<Course>();
                    courses = pr.Courses.ToList();
                    if(courses != null)
                    {
                        double gpa = 0.0;
                        double gradesmultiply = 0.0;
                        double totalgrades = 0.0;
                        double totalCreditHour=0;
                        foreach(Course cs in courses)
                        {
                            double gradepoint = 0.0;
                            if (cs.CurrentGrade >= 80.00)
                            {
                                gradepoint = 4.0;
                            }
                            else if (cs.CurrentGrade >= 75.00 && cs.CurrentGrade < 80.00)
                            {
                                gradepoint = 3.5;
                            }
                            else if (cs.CurrentGrade >= 70 && cs.CurrentGrade <75.00)
                            {
                                gradepoint = 3.0;
                            }
                            else if (cs.CurrentGrade >= 65.00 && cs.CurrentGrade < 70.00)
                            {
                                gradepoint = 2.5;
                            }
                            else if (cs.CurrentGrade >= 60.00 && cs.CurrentGrade < 65.00)
                            {
                                gradepoint = 2.0;
                            }
                            else if (cs.CurrentGrade >= 50.00 && cs.CurrentGrade < 60.00)
                            {
                                gradepoint = 1.0;
                            }
                            else { gradepoint = 0.0; }
                            // Multiply each numeric grade value by the number of credits the course was worth
                            gradesmultiply = (cs.Credits * gradepoint);
                            //Add these numbers together
                            totalgrades = totalgrades + gradesmultiply;
                             totalCreditHour = totalCreditHour + cs.Credits;
                           
                        }
                        
                        //Divide by the total number of credits you took
                        gpa = (totalgrades / totalCreditHour);
                        st.CurrentGPA = gpa;
                        db.SaveChanges();
                    }
                }
                
            }
            return PartialView(st);

        }
        public JsonResult GetStudentData()
        {
            int id = (int)Session["StudentId"];
            Student st = db.Students.Find(id);
            var list = JsonConvert.SerializeObject(st, Formatting.None,
                    new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
