﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StudentDash.Models;

namespace StudentDash.Controllers
{
    public class GradesController : Controller
    {
        private DBModelContainer db = new DBModelContainer();

        // GET: Grades
        public ActionResult Index()
        {
            var grades = db.Grades.Include(g => g.Course);
            return View(grades.ToList());
        }
        public ActionResult updatestudentgrade(int? id)
        {
            GradeVM gvm = new GradeVM();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grade gr = new Grade();
            gr = db.Grades.Find(id);
            if (gr != null)
            {
                gvm.grade = gr;
                Course cs = new Course();
                cs = db.Courses.Find(gr.CourseId);
                if (cs != null)
                {
                    
                    Program pr = new Program();
                    int progragmID = cs.Program.Id;
                    pr = db.Programs.Find(progragmID);
                    if (pr != null)
                    {
                        if (User.IsInRole("Admin"))
                        {
                            gvm.StudentList = pr.Students.ToList();
                        }
                        else
                        {
                            Student st = new Student();
                            int Studentid = (int)Session["StudentId"];
                            st = db.Students.Find(Studentid);
                            if (st != null)
                            {
                                if (st.Programs.Contains(pr))
                                {
                                    gvm.StudentList.Add(st);
                                }
                            }
                        }
                       
                    }
                }
            }

            ViewBag.StudentId = new SelectList(gvm.StudentList, "Id", "FirstName");
            return View(gvm);
        }

        [HttpPost]
        public ActionResult updatestudentgrade(GradeVM gvm)
        {
            if (ModelState.IsValid)
            {
               
                int studentid = Int32.Parse(Request.Form["StudentId"].ToString());
                int marks = (int)gvm.grade.AcheivedMarks;
                StudentGrade sg = new StudentGrade();
                sg.Grades_Id = gvm.grade.Id;
                sg.Students_Id = studentid;
                sg.AcheivedMarks = marks;
                var recordExist = db.StudentGrades.Where(a => a.Grades_Id == sg.Grades_Id && a.Students_Id == sg.Students_Id).FirstOrDefault();
                if (recordExist != null)
                {
                    recordExist.Students_Id = sg.Students_Id;
                    recordExist.Grades_Id = sg.Grades_Id;
                    recordExist.AcheivedMarks = sg.AcheivedMarks;
                    db.SaveChanges();
                    TempData["UserMessage"] = "Student Grade Updated";
                }
                
                else
                {
                    db.StudentGrades.Add(sg);
                    db.SaveChanges();
                    TempData["UserMessage"] = "Student Grade Updated";
                }
               
            }
            return RedirectToAction("updatestudentgrade", "Grades");
        }
        [HttpPost]
        public JsonResult getMarks(int studentid, int gradeid)
        {
            int acheivedmarks = 0;
            var recordExist = db.StudentGrades.Where(a => a.Grades_Id == gradeid && a.Students_Id == studentid).FirstOrDefault();
            if (recordExist != null) {  acheivedmarks = (int)recordExist.AcheivedMarks; }
                Grade gr = new Grade
            {
                Id = gradeid,
                StudentId=studentid,
                AcheivedMarks=acheivedmarks
            };
            return Json(gr);
        }
        // GET: Grades/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grade grade = db.Grades.Find(id);
            if (grade == null)
            {
                return HttpNotFound();
            }
            return View(grade);
        }

        // GET: Grades/Create
        public ActionResult Create()
        {
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Name");
            return View();
        }

        // POST: Grades/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,GradeName,GradeType,TotalMarks,AcheivedMarks,Weight,CourseId,StudentId")] Grade grade)
        {
            if (ModelState.IsValid)
            {
                //check total of all the grades should not be greater then 100
                if (CheckTotalWeight(grade.CourseId, grade.Weight))
                {
                    db.Grades.Add(grade);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                
            }

            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Name", grade.CourseId);
            return View(grade);
        }

        // GET: Grades/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grade grade = db.Grades.Find(id);
            if (grade == null)
            {
                return HttpNotFound();
            }
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Name", grade.CourseId);
            return View(grade);
        }

        // This Method check for total weight and return true or false 
        public Boolean CheckTotalWeight( int ID ,Double EnteredWeight)
        {
            Course c = db.Courses.Find(ID);
            List<Grade> grades = c.Grades.ToList();
            int total_weight = 0;
            total_weight = total_weight + (int)EnteredWeight;
            foreach (Grade g in grades)
            {
                total_weight += (int)g.Weight;
            }
            if (total_weight > 100)
            {
                int t2 = total_weight - 100; // 123-100
                ModelState.AddModelError("Weight", "You are Exceeding " + t2 + " Marks");
                return false;
            }
            else
            {
                return true;
            }
        }
        // POST: Grades/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,GradeName,GradeType,TotalMarks,AcheivedMarks,Weight,CourseId,StudentId")] Grade grade)
        {
            if (ModelState.IsValid)
            {
                db.Entry(grade).State = EntityState.Modified;
                if (CheckTotalWeight(grade.CourseId, 0))
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                
            }
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Name", grade.CourseId);
            return View(grade);
        }

        // GET: Grades/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grade grade = db.Grades.Find(id);
            if (grade == null)
            {
                return HttpNotFound();
            }
            return View(grade);
        }

        // POST: Grades/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Grade grade = db.Grades.Find(id);
            db.Grades.Remove(grade);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
