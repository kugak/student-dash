﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IdentitySample.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using StudentDash.Models;
using Twilio.Rest;

namespace StudentDash.Controllers
{
    public class EventsController : Controller
    {
        private DBModelContainer db = new DBModelContainer();
        // GET: Events
        public ActionResult Index()
        {
            //check if user logged in
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.ID = (int)Session["StudentId"];// use this variable in ajax request to update or delete
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
       
        [Authorize]
        public void Execute()
        {
            List<Event> events = new List<Event>();
            events = db.Events.ToList();
            if (events != null)
            {
                foreach (Event ev in events)
                {    
                    DateTime StartTime = (DateTime)ev.Start;
                    DateTime EndTime = (DateTime)ev.End;
                    DateTime TimeNow = DateTime.Now;
                    // if Event has passed date and time then delete 
                    if (DateTime.Compare(EndTime, TimeNow) < 0)
                    {
                        db.Events.Remove(ev);
                        
                    }
                    // if the event is not reminded then send reminder
                    else if (ev.IsReminded == false)
                    {
                        TimeSpan diff = StartTime.Subtract(TimeNow);
                        int minutes = (int)Math.Round(diff.TotalMinutes);
                        if (minutes < 30 && ev.PhoneNumber!=null)
                        {
                            string MessageTemplate = "Hi Just a reminder that you have an Event  "+ev.Subject+ " coming up at "+ev.Start  ;
                            var twilioRestClient = new RestClient();
                             twilioRestClient.SendSmsMessage(
                             ev.PhoneNumber,
                             string.Format(MessageTemplate));
                            ev.IsReminded = true;
                            
                        }
                    }
                    db.SaveChanges();
                }
            }
            
        
        }

        //Use this Method As Partial View 
        public ActionResult EventsList()
        {
            int studentId = (int)Session["StudentId"];
            Student st = new Student();
            st = db.Students.Find(studentId);
            List<Event> events = new List<Event>();
            events = st.Events.ToList();
            return PartialView(events);
        }
        public JsonResult GetEvents(int id)
        {
            Student st = db.Students.Find(id);
                var events = st.Events.ToList();
            var list = JsonConvert.SerializeObject(events,Formatting.None,new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            return new JsonResult { Data = list, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
           
        }

        [HttpPost]
        public JsonResult SaveEvent(Event e)
        {
            var status = false;
           
                if (e.EventID > 0)
                {
                    //Update the event
                    var v = db.Events.Where(a => a.EventID == e.EventID).FirstOrDefault();
                    if (v != null)
                    {
                        v.Subject = e.Subject;
                        v.Start = e.Start;
                        v.End = e.End;
                        v.Description = e.Description;
                        v.IsFullDay = e.IsFullDay;
                        v.ThemeColor = e.ThemeColor;
                    v.Location = e.Location;
                    v.RoomNo = e.RoomNo;
                    v.PhoneNumber = e.PhoneNumber;
                     v.IsReminded = false;
                    }
                }
                else
                {
                e.StudentId = (int)Session["StudentId"];
                db.Events.Add(e);
                }

                db.SaveChanges();
                status = true;
            return new JsonResult { Data = new { status = status } };
        }

        [HttpPost]
        public JsonResult DeleteEvent(int eventID)
        {
            var status = false;
            
                var v = db.Events.Where(a => a.EventID == eventID).FirstOrDefault();
                if (v != null)
                {
                    db.Events.Remove(v);
                    db.SaveChanges();
                    status = true;
                }
            
            return new JsonResult { Data = new { status = status } };
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
