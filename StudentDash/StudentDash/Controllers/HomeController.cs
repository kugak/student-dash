﻿using StudentDash.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace StudentDash.Controllers
{
    public class HomeController : Controller
    {
        DBModelContainer db = new DBModelContainer();
        public ActionResult Index()
        {
            if (Request.IsAuthenticated) {
                
                // Check if user is assigned to program
                int studentId = (int)Session["StudentId"];
                Student st = new Student();
                st = db.Students.Find(studentId);
                int totalPrograms = st.Programs.Count;
                //if user is not enrolled in any course then take to enroll
                if (totalPrograms > 0)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("selectprogram", "Programs");

                }
                
            }
            else
            {
                return RedirectToAction("LogIn", "Account");
            }
        }

        public ActionResult AnotherLink()
        {
            return View("Index");
        }
    }
}
