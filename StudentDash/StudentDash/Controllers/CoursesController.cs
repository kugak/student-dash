﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using StudentDash.Models;

namespace StudentDash.Controllers
{
    public class CoursesController : Controller
    {
        private DBModelContainer db = new DBModelContainer();

        // GET: Courses
        public ActionResult Index()
        {
            if (User.IsInRole("Admin"))
            {

                var Allcourses = db.Courses.Include(c => c.Program);
                return View(Allcourses.ToList());
            }
            else {
                int StudentId = (int)Session["StudentId"];
                List<Course> coursesList = new List<Course>();
                List<Course> finalList = new List<Course>();
                Student st = new Student();
                st = db.Students.Find(StudentId);
                Program pr = new Program();
                pr = st.Programs.First();
                var courses = db.Courses.Include(c => c.Program);
                if (courses != null)
                {
                    coursesList = courses.ToList();
                    foreach (Course cs in coursesList)
                    {
                        if (cs.ProgramId == pr.Id)
                        {
                            finalList.Add(cs);
                        }
                    }
                    
                }
                return View(finalList);
            }
           
        }

        //This method display drop down list of courses on front page
        // user will see chart on front page
        public ActionResult getStudentCourses()
        {
            int studentid = (int)Session["StudentId"];
            Student st = new Student();
            st = db.Students.Find(studentid);
            if (st != null)
            {
                Program pr = new Program();
                pr = st.Programs.First();
                if (pr != null)
                {
                    
                    ViewBag.CourseId = new SelectList(pr.Courses.ToList(), "Id", "Name");

                }
            }
            return View();
        }
        [HttpPost]
        // this method return data to display in chart 
        public JsonResult getStudentGrades(int? courseid)
        {

            int studentid = (int)Session["StudentId"];         
            List<StudentGrade> studentgrades = new List<StudentGrade>();
            List<DataPoint> dataPointsgain = new List<DataPoint>();
            List<Grade> grades = new List<Grade>();
            Course cs = new Course();
            cs = db.Courses.Find(courseid);
            if (cs != null)
            {
                grades = cs.Grades.ToList();
            }
            // get all grades from database
            studentgrades = db.StudentGrades.ToList();
            foreach (Grade grade in grades)
            {
                foreach (StudentGrade gr in studentgrades)
                {


                    // search for single student grade 
                    if (gr.Students_Id == studentid && gr.Grades_Id==grade.Id)
                    {
                        //Grade grade = new Grade();
                        //grade = db.Grades.Find(gr.Grades_Id);

                        double acheivedmarks = (double)gr.AcheivedMarks;
                        double totalmarks = grade.TotalMarks;
                        double marksDeducted = (totalmarks - acheivedmarks);
                        dataPointsgain.Add(new DataPoint(acheivedmarks, marksDeducted));

                    }
                }
            }
           
            var result = new { data1 = JsonConvert.SerializeObject(dataPointsgain) };
            return Json(result, JsonRequestBehavior.AllowGet);
            
        }
        // this method will return grades in that course
        
        // GET: Courses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = db.Courses.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            int StudentId = (int)Session["StudentId"];
            List<Grade> grades = new List<Grade>();
            grades = course.Grades.ToList();
            List<StudentGrade> studentgradeList = new List<StudentGrade>();
            studentgradeList = db.StudentGrades.ToList();
            foreach (Grade gr in grades)
            {
                foreach (StudentGrade sg in studentgradeList)
                {

                    if (gr.Id == sg.Grades_Id && StudentId == sg.Students_Id)
                    {
                        gr.AcheivedMarks = sg.AcheivedMarks;
                    }

                }
            }
            return View(grades);
        }

        // GET: Courses/Create
        public ActionResult Create()
        {
            ViewBag.ProgramId = new SelectList(db.Programs, "Id", "Name");
            return View();
        }

        // POST: Courses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Code,Credits,ProgramId,CurrentGrade")] Course course)
        {
            if (ModelState.IsValid)
            {
                db.Courses.Add(course);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProgramId = new SelectList(db.Programs, "Id", "Name", course.ProgramId);
            return View(course);
        }

        // GET: Courses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = db.Courses.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProgramId = new SelectList(db.Programs, "Id", "Name", course.ProgramId);
            return View(course);
        }

        // POST: Courses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Code,Credits,ProgramId,CurrentGrade")] Course course)
        {
            if (ModelState.IsValid)
            {
                db.Entry(course).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProgramId = new SelectList(db.Programs, "Id", "Name", course.ProgramId);
            return View(course);
        }

        // GET: Courses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = db.Courses.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        // POST: Courses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Course course = db.Courses.Find(id);
            db.Courses.Remove(course);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
