namespace StudentDash.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class studentdash2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "StudentFirstName", c => c.String());
            AddColumn("dbo.AspNetUsers", "StudentLastName", c => c.String());
            AddColumn("dbo.AspNetUsers", "StudentIdentity", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "StudentIdentity");
            DropColumn("dbo.AspNetUsers", "StudentLastName");
            DropColumn("dbo.AspNetUsers", "StudentFirstName");
        }
    }
}
