![StudentDash](StudentDash/StudentDash/Content/img/dash-logo.png)

# README #

Student Dash Repository.

### What is this repository for? ###

* This repo is for Student-Dash
* Version 1.0
* This repo requires Visual Studio 2017 to run locally
* This site is hosted on Azure. https://student-dash.azurewebsites.net

### How do I get set up? ###

* Summary of set up
	+ Install visual studio 2017
	+ Set up git repository to work with BitBucket (Visual Studio Bitbucket Extension)
	
* Configuration
	1. Pull from the latest tag
	2. You may want to update the Nuget Packages for solution
		+ Go to "Tools" > "Nuget Package Manager" > "Manage Nuget Package for Solution" 
		+ You may see a yellow ribbon to "Restore" if avaiable
		+ Rebuild Project

* Initial Set up of Database (required step for local and server first time run)
	There are 2 steps for the database to be fully generated.
	1. Run the project (this will create the identity tables, required for user login and authentication)
	2. Stop the project.
	3. Connect to the database and run the query to create our tables required for this project.
		+ The scripts can be found in the wiki page "How to Fix Database model Error".
		+ After running the script, the application should function as intended.
	 
### Contribution guidelines ###

* Code review
* Naming Conventions
  + use dashes to name any of the branches
  + to develop create a branch off master
  + name the branch with the summary of the feature you will be working on and your name
  	+ for example - working on database feature branch would be called 
  		+ db-model-addition-kuga
  		+ db-model-addition feature and kuga is the person working on

### Who do I talk to? ###

* Kugakanth Kugarajasingam
* Lokesh Chamane
* Mohammed Riaz
* Kajeepan Srikanthan